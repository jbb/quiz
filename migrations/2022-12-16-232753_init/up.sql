create table quizzes (
    id Blob primary key not null,
    title Text not null
);
create table questions (
    id Blob primary key not null,
    quiz_id Blob not null,
    slide_number Int not null,
    question Text not null,
    duration Int not null,
    image Text,
    foreign key (quiz_id) references quizzes (id)
);
create table answers (
    id Blob primary key not null,
    question_id Blob not null,
    text Text not null,
    correct Boolean not null,
    position Int not null,
    foreign key (question_id) references questions (id)
);
create table participant_links (
    id Blob primary key,
    quiz_id Blob not null,
    foreign key (quiz_id) references quiz (id)
);
