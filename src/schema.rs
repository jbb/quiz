// @generated automatically by Diesel CLI.

diesel::table! {
    answers (id) {
        id -> Binary,
        question_id -> Binary,
        text -> Text,
        correct -> Bool,
        position -> Integer,
    }
}

diesel::table! {
    participant_links (id) {
        id -> Nullable<Binary>,
        quiz_id -> Binary,
    }
}

diesel::table! {
    questions (id) {
        id -> Binary,
        quiz_id -> Binary,
        slide_number -> Integer,
        question -> Text,
        duration -> Integer,
        image -> Nullable<Text>,
    }
}

diesel::table! {
    quizzes (id) {
        id -> Binary,
        title -> Text,
    }
}

diesel::joinable!(answers -> questions (question_id));
diesel::joinable!(questions -> quizzes (quiz_id));

diesel::allow_tables_to_appear_in_same_query!(answers, participant_links, questions, quizzes,);
