use rocket::{
    delete,
    form::Form,
    fs::TempFile,
    get,
    http::Cookie,
    post,
    request::{FromRequest, Outcome, Request},
    serde::json::Json,
};

use std::time::Duration;

use crate::*;

pub struct AuthenticatedUser {}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for AuthenticatedUser {
    type Error = ();

    async fn from_request(_req: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        Outcome::Success(AuthenticatedUser {})
    }
}

pub struct Session {
    player_id: Uuid,
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for Session {
    type Error = ();

    async fn from_request(req: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        match req.cookies().get("SESSION_ID") {
            Some(session_id) => Outcome::Success(Session {
                player_id: Uuid::parse_str(&session_id.value()).unwrap(),
            }),
            None => {
                let player_id = Uuid::new_v4();
                req.cookies()
                    .add(Cookie::new("SESSION_ID", player_id.to_string()));
                Outcome::Success(Session { player_id })
            }
        }
    }
}

#[get("/can_join?<game_id>")]
pub fn can_join(state: &GlobalState, game_id: u32) -> &str {
    let state_guard = state.read().unwrap();
    if state_guard.contains_key(&game_id) {
        "true"
    } else {
        "false"
    }
}

#[post("/edit/<quiz_id>/title", data = "<title>")]
pub async fn set_title(user: AuthenticatedUser, db: Database, quiz_id: &str, title: &str) {
    let id = Uuid::parse_str(quiz_id).unwrap();
    db.set_title(id, title.to_string()).await.unwrap();
}

#[post("/edit/<quiz_id>/question")]
pub async fn add_question(user: AuthenticatedUser, db: Database, quiz_id: String) -> String {
    let id = Uuid::parse_str(&quiz_id).unwrap();
    let question_id = db
        .insert_question(&id, String::new(), 30)
        .await
        .expect("Insert question");
    question_id.to_string()
}

#[delete("/edit/question/<question_id>")]
pub async fn delete_question(user: AuthenticatedUser, db: Database, question_id: String) {
    let id = Uuid::parse_str(&question_id).unwrap();
    db.delete_question(id).await.unwrap();
}

#[post("/edit/<quiz_id>/<question_id>/title", data = "<title>")]
pub async fn set_question_title(
    user: AuthenticatedUser,
    db: Database,
    quiz_id: String,
    question_id: String,
    title: String,
) {
    let id = Uuid::parse_str(&question_id).unwrap();
    db.set_question_title(id, title.clone()).await.unwrap();
    println!("Editing title of question ({question_id}) in quiz ({quiz_id}) to \"{title}\"");
}

pub const MEDIA_LOCATION: &str = "media";

#[derive(FromForm)]
pub struct FileUpload<'f> {
    file: TempFile<'f>,
}

#[post("/edit/<quiz_id>/<question_id>/image", data = "<form>")]
pub async fn set_question_image(
    user: AuthenticatedUser,
    db: Database,
    quiz_id: String,
    question_id: String,
    mut form: Form<FileUpload<'_>>,
) -> String {
    let media_id = Uuid::new_v4().to_string();
    let extension = form
        .file
        .content_type()
        .unwrap()
        .extension()
        .unwrap()
        .as_str()
        .to_string();
    form.file
        .copy_to(&format!("media/{media_id}.{extension}"))
        .await
        .unwrap();

    let image_name = format!("{media_id}.{extension}");

    let id = Uuid::parse_str(&question_id).unwrap();
    db.set_question_image(id, image_name.clone()).await.unwrap();
    return image_name;
}

#[delete("/edit/<quiz_id>/<question_id>/image", data = "<form>")]
pub async fn remove_question_image(
    user: AuthenticatedUser,
    db: Database,
    quiz_id: String,
    question_id: String,
    mut form: Form<FileUpload<'_>>,
) {
    let question_id = Uuid::parse_str(&question_id).unwrap();
    if let Some(image_id) = db.get_question(question_id).await.unwrap().image {
        fs::remove_file(format!("{MEDIA_LOCATION}/{image_id}")).unwrap();
    }
    db.delete_question_image(question_id).await.unwrap();
}

#[post("/edit/<quiz_id>/<question_id>/slide", data = "<number>")]
pub async fn set_question_slide(
    user: AuthenticatedUser,
    db: Database,
    quiz_id: String,
    question_id: String,
    number: &str,
) {
    let question_id = Uuid::parse_str(&question_id).unwrap();

    db.set_question_slide(question_id, number.parse().unwrap())
        .await
        .unwrap();
}

// change question duration
#[post("/edit/<quiz_id>/<question_id>/duration", data = "<duration>")]
pub async fn set_question_duration(
    user: AuthenticatedUser,
    db: Database,
    quiz_id: String,
    question_id: String,
    duration: &str,
) {
    let question_id = Uuid::parse_str(&question_id).unwrap();

    // TODO
    println!("Set question duration to {duration}");
}

// change question type
#[post("/edit/<quiz_id>/<question_id>/type/<question_type>")]
pub async fn set_question_type(
    user: AuthenticatedUser,
    db: Database,
    quiz_id: String,
    question_id: String,
    question_type: &str,
) {
    let question_id = Uuid::parse_str(&question_id).unwrap();

    // TODO
    println!("Set question type to {question_type}");
    // db.set_question_type(question_id, question_type)
    //     .await
    //     .unwrap();
}

#[post("/edit/<quiz_id>/<question_id>/answer/<position>", data = "<answer>")]
pub async fn add_answer(
    user: AuthenticatedUser,
    db: Database,
    quiz_id: String,
    question_id: String,
    position: i32,
    answer: String,
) {
    let question_id = Uuid::parse_str(&question_id).unwrap();

    db.set_answer_text(question_id, position, answer)
        .await
        .unwrap();
}

#[post("/edit/<quiz_id>/<question_id>/true-or-false", data = "<true_or_false>")]
pub async fn set_question_true_or_false(
    user: AuthenticatedUser,
    db: Database,
    quiz_id: String,
    question_id: String,
    true_or_false: &str,
) {
    let question_id = Uuid::parse_str(&question_id).unwrap();

    // TODO
    println!("Set question true or false to {true_or_false}");
    // db.set_question_true_or_false(question_id, true_or_false)
    //     .await
    //     .unwrap();
}


#[post("/edit/<quiz_id>/<question_id>/correct-answers", data = "<correct_answers>")]
pub async fn set_correct_answers(
    user: AuthenticatedUser,
    db: Database,
    quiz_id: String,
    question_id: String,
    correct_answers: &str,
) {
    let question_id = Uuid::parse_str(&question_id).unwrap();
    // TODO
    for position in correct_answers.trim().split("") {
        println!("Set answer {position} to correct");
    }
    // db.set_answer_correct(question_id, position, true)
    //     .await
    //     .unwrap();
}

#[derive(Serialize)]
pub struct Slide {
    slide_number: i32,
    question: String,
    duration: i32,
    image: Option<String>,
}

#[get("/game/<game_id>/current_slide")]
pub async fn get_current_slide(db: Database, game_id: u32, state: &GlobalState) -> Json<Slide> {
    let (quiz_id, current_slide) = {
        let state = state.read().unwrap();
        let game = state.get(&game_id).unwrap();
        let quiz_id = game.quiz_id;
        (quiz_id, game.current_slide)
    };
    let QuestionEntity {
        id: _,
        quiz_id: _,
        slide_number,
        question,
        duration,
        image,
    } = db.get_slide(quiz_id, current_slide).await.unwrap();
    Json(Slide {
        slide_number,
        question,
        duration,
        image,
    })
}

#[post("/game/<game_id>/start")]
pub async fn start_game(user: AuthenticatedUser, game_id: u32, state: &GlobalState) {
    // TODO verify user is host

    let mut writable_state = state.write().unwrap();
    let game = writable_state.get_mut(&game_id).unwrap();

    game.slide_start_time = SystemTime::now();
    game.status = GameStatus::Running;

    game.event_sender.send(GameEvent::Started).unwrap();
}

#[post("/game/<game_id>/next")]
pub async fn next_slide(user: AuthenticatedUser, game_id: u32, state: &GlobalState) {
    // TODO verify used is host

    let mut writable_state = state.write().unwrap();
    let game = writable_state.get_mut(&game_id).unwrap();

    game.current_slide += 1;
    game.event_sender.send(GameEvent::NextSlide).unwrap();
}

#[post("/game/<game_id>/answer", data = "<answer_id>")]
pub async fn answer_question(
    db: Database,
    game_id: u32,
    state: &GlobalState,
    answer_id: String,
    session: Session,
) {
    let (current_slide, slide_start_time, quiz_id) = {
        let mut writable_state = state.write().unwrap();
        let game = writable_state.get_mut(&game_id).unwrap();

        (game.current_slide, game.slide_start_time, game.quiz_id)
    };

    let answer_id = Uuid::parse_str(&answer_id).unwrap();
    let correct = db.get_answer(answer_id).await.unwrap().correct;

    let question = db.get_slide(quiz_id.clone(), current_slide).await.unwrap();
    let question_id = Uuid::from_slice(&question.id).unwrap();

    if correct {
        let answer_time = SystemTime::now()
            .duration_since(slide_start_time)
            .unwrap()
            .as_millis();
        let max_answer_time = Duration::from_secs(question.duration as u64).as_millis();
        let max_points = 100;

        let points =
            ((1.0 - (answer_time as f32 / max_answer_time as f32)) * max_points as f32) as u32;
        println!("{points} Points");

        let mut state = state.write().unwrap();
        let game = state.get_mut(&game_id).unwrap();
        let player = game.players.get_mut(&session.player_id).unwrap();

        if !player.answered_questions.contains(&question_id) {
            player.points += points;
            player.answered_questions.push(question_id);
        }
    } else {
        let mut state = state.write().unwrap();
        let game = state.get_mut(&game_id).unwrap();
        let player = game.players.get_mut(&session.player_id).unwrap();

        if !player.answered_questions.contains(&question_id) {
            player.answered_questions.push(question_id);
        }
    }
}
