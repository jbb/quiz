// SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use crate::*;

use serde::Serialize;

use rocket_sync_db_pools::{database, diesel::SqliteConnection};

use std::cmp::Ordering::{Equal, Greater, Less};

#[derive(Serialize, Clone)]
pub struct Question {
    title: String,
    id: String,
    image: Option<String>,
    answers: Vec<Answer>,
}

#[database("quiz")]
pub struct Database(SqliteConnection);

impl Database {
    pub async fn get_quiz(&self, quiz_id: Uuid) -> QueryResult<QuizEntity> {
        self.run(move |c| {
            quizzes::table
                .filter(quizzes::id.eq(quiz_id.as_bytes().to_vec()))
                .get_result::<QuizEntity>(c)
        })
        .await
    }

    pub async fn get_questions(&self, quiz_id: Uuid) -> Vec<Question> {
        self.run(move |c| {
            let questions = questions::table
                .filter(questions::quiz_id.eq(quiz_id.as_bytes().to_vec()))
                .load::<QuestionEntity>(c)
                .unwrap();

            let mut q = Vec::new();
            for question in questions {
                let answers = answers::table
                    .filter(answers::question_id.eq(&question.id))
                    .load::<AnswerEntity>(c)
                    .unwrap();

                q.push(Question {
                    title: question.question,
                    id: Uuid::from_slice(&question.id).unwrap().to_string(),
                    image: question.image,
                    answers: answers
                        .into_iter()
                        .map(|a| Answer {
                            text: a.text,
                            id: Uuid::from_slice(&a.id).unwrap().to_string(),
                        })
                        .collect(),
                })
            }
            q
        })
        .await
    }

    pub async fn set_title(&self, quiz_id: Uuid, title: String) -> QueryResult<()> {
        self.run(move |c| {
            diesel::update(quizzes::table)
                .filter(quizzes::id.eq(quiz_id.as_bytes().as_slice().to_vec()))
                .set(quizzes::title.eq(title))
                .execute(c)
        })
        .await
        .unwrap();

        Ok(())
    }

    pub async fn set_question_title(&self, question_id: Uuid, title: String) -> QueryResult<()> {
        self.run(move |c| {
            diesel::update(questions::table)
                .filter(questions::id.eq(question_id.as_bytes().as_slice().to_vec()))
                .set(questions::question.eq(title))
                .execute(c)
        })
        .await
        .unwrap();

        Ok(())
    }

    pub async fn add_quiz(&self, title: String) -> QueryResult<Uuid> {
        let uuid = Uuid::new_v4();
        let _uuid_string = uuid.to_string();

        self.run(move |c| {
            diesel::insert_into(quizzes::table)
                .values(QuizEntity {
                    id: uuid.as_bytes().to_vec(),
                    title,
                })
                .execute(c)
        })
        .await
        .unwrap();

        self.insert_question(&uuid, String::new(), 0).await.unwrap();

        Ok(uuid)
    }

    pub async fn insert_question(
        &self,
        quiz_id: &Uuid,
        question: String,
        duration: i32,
    ) -> QueryResult<Uuid> {
        let qid = quiz_id.as_bytes().to_vec();
        let question_id = Uuid::new_v4();
        let id_bytes = question_id.as_bytes().to_vec();

        self.run(move |c| {
            let last_slide = questions::table
                .filter(questions::quiz_id.eq(&qid))
                .select(questions::slide_number)
                .order_by(questions::slide_number.desc())
                .limit(1)
                .get_result::<i32>(c)
                .unwrap_or(-1);

            diesel::insert_into(questions::table)
                .values(QuestionEntity {
                    id: id_bytes,
                    quiz_id: qid,
                    slide_number: last_slide + 1,
                    question,
                    duration,
                    image: None,
                })
                .execute(c)
        })
        .await?;

        for i in 0..4 {
            self.insert_answer(question_id, String::new(), false, i)
                .await
                .unwrap();
        }

        Ok(question_id)
    }

    pub async fn delete_question(&self, question_id: Uuid) -> QueryResult<()> {
        self.run(move |c| -> QueryResult<usize> {
            let question_id = question_id.as_bytes().to_vec();
            diesel::delete(answers::table)
                .filter(answers::question_id.eq(&question_id))
                .execute(c)?;
            diesel::delete(questions::table)
                .filter(questions::id.eq(&question_id))
                .execute(c)
        })
        .await
        .expect("delete question");
        Ok(())
    }

    pub async fn set_question_slide(
        &self,
        question_id: Uuid,
        new_slide_number: i32,
    ) -> QueryResult<()> {
        self.run(move |c| {
            let question_id = question_id.as_bytes().to_vec();

            c.transaction(|c| {
                let old_position: i32 = questions::table
                    .filter(questions::id.eq(&question_id))
                    .select(questions::slide_number)
                    .limit(1)
                    .get_result(c)
                    .unwrap();
                match old_position.cmp(&new_slide_number) {
                    Less => {
                        diesel::update(questions::table)
                            .filter(
                                questions::slide_number
                                    .gt(old_position)
                                    .and(questions::slide_number.le(new_slide_number)),
                            )
                            .set(questions::slide_number.eq(questions::slide_number - 1))
                            .execute(c)
                            .unwrap();
                    }
                    Greater => {
                        diesel::update(questions::table)
                            .filter(
                                questions::slide_number
                                    .ge(new_slide_number)
                                    .and(questions::slide_number.lt(old_position)),
                            )
                            .set(questions::slide_number.eq(questions::slide_number + 1))
                            .execute(c)
                            .unwrap();
                    }
                    Equal => {}
                }

                diesel::update(questions::table)
                    .filter(questions::id.eq(&question_id))
                    .set(questions::slide_number.eq(new_slide_number))
                    .execute(c)
            })
        })
        .await
        .expect("delete question");
        Ok(())
    }

    pub async fn set_question_image(&self, question: Uuid, image: String) -> QueryResult<()> {
        self.run(move |c| {
            diesel::update(questions::table)
                .filter(questions::id.eq(question.as_bytes().to_vec()))
                .set(questions::image.eq(image))
                .execute(c)
        })
        .await?;
        Ok(())
    }

    pub async fn delete_question_image(&self, question: Uuid) -> QueryResult<()> {
        self.run(move |c| {
            diesel::update(questions::table)
                .filter(questions::id.eq(question.as_bytes().to_vec()))
                .set(questions::image.eq(Option::<String>::None))
                .execute(c)
        })
        .await?;
        Ok(())
    }

    pub async fn get_question(&self, question: Uuid) -> QueryResult<QuestionEntity> {
        self.run(move |c| {
            questions::table
                .filter(questions::id.eq(question.as_bytes().to_vec()))
                .get_result(c)
        })
        .await
    }

    pub async fn insert_answer(
        &self,
        question_id: Uuid,
        answer: String,
        correct: bool,
        position: i32,
    ) -> QueryResult<Uuid> {
        let answer_id = Uuid::new_v4();
        self.run(move |c| {
            let question_id = question_id.as_bytes().to_vec();

            diesel::insert_into(answers::table)
                .values(AnswerEntity {
                    id: answer_id.as_bytes().to_vec(),
                    question_id,
                    text: answer,
                    correct,
                    position,
                })
                .execute(c)
        })
        .await
        .unwrap();

        Ok(answer_id)
    }

    pub async fn set_answer_text(
        &self,
        question_id: Uuid,
        position: i32,
        text: String,
    ) -> QueryResult<()> {
        self.run(move |c| {
            diesel::update(answers::table)
                .filter(
                    answers::question_id
                        .eq(question_id.as_bytes().to_vec())
                        .and(answers::position.eq(position)),
                )
                .set(answers::text.eq(text))
                .execute(c)
        })
        .await
        .unwrap();

        Ok(())
    }

    pub async fn get_answers(&self, question_id: Uuid) -> QueryResult<Vec<AnswerEntity>> {
        self.run(move |c| {
            answers::table
                .filter(answers::question_id.eq(question_id.as_bytes().to_vec()))
                .load::<AnswerEntity>(c)
        })
        .await
    }

    pub async fn create_public_link(&self, qid: Uuid) -> QueryResult<Uuid> {
        let uuid = Uuid::new_v4();
        self.run(move |c| {
            diesel::insert_into(participant_links::table)
                .values(ParticipantLink {
                    id: uuid.as_bytes().to_vec(),
                    quiz_id: qid.as_bytes().to_vec(),
                })
                .execute(c)
        })
        .await?;
        Ok(uuid)
    }

    pub async fn get_slide(&self, qid: Uuid, slide: i32) -> QueryResult<QuestionEntity> {
        self.run(move |c| {
            questions::table
                .filter(questions::quiz_id.eq(qid.as_bytes().to_vec()))
                .filter(questions::slide_number.eq(slide))
                .get_result::<QuestionEntity>(c)
        })
        .await
    }

    pub async fn get_answer(&self, answer_id: Uuid) -> QueryResult<AnswerEntity> {
        self.run(move |c| {
            answers::table
                .filter(answers::id.eq(answer_id.as_bytes().to_vec()))
                .get_result::<AnswerEntity>(c)
        })
        .await
    }
}
