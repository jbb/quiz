// SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use rocket::{form::FromForm, fs::FileServer, launch, routes, State};

use diesel::prelude::*;

use rocket_dyn_templates::Template;

use serde::{Deserialize, Serialize};

use uuid::Uuid;

use std::{
    collections::HashMap,
    fs,
    sync::{Arc, RwLock},
    time::SystemTime,
};

use rocket::tokio::sync::broadcast::{Receiver, Sender};

mod api;
mod database;
mod schema;
mod ui;
use schema::*;

use database::Database;

#[derive(FromForm, Deserialize, Clone)]
pub struct AnswerForm {
    text: String,
    position: i32,
    correct: bool,
}

#[derive(Insertable, Queryable)]
#[diesel(table_name = questions)]
pub struct QuestionEntity {
    id: Vec<u8>,
    quiz_id: Vec<u8>,
    slide_number: i32,
    question: String,
    duration: i32,
    image: Option<String>,
}

#[derive(Serialize, Clone)]
pub struct Answer {
    text: String,
    id: String,
}

#[derive(Insertable, Queryable, Debug)]
#[diesel(table_name = answers)]
pub struct AnswerEntity {
    id: Vec<u8>,
    question_id: Vec<u8>,
    text: String,
    correct: bool,
    position: i32,
}

#[derive(Insertable, Queryable)]
#[diesel(table_name = quizzes)]
pub struct QuizEntity {
    id: Vec<u8>,
    title: String,
}

#[derive(Insertable)]
pub struct ParticipantLink {
    id: Vec<u8>,
    quiz_id: Vec<u8>,
}

pub type GlobalData = Arc<RwLock<HashMap<u32, GameState>>>;
pub type GlobalState = State<GlobalData>;

pub struct PlayerState {
    name: String,
    points: u32,
    answered_questions: Vec<Uuid>,
}

enum GameStatus {
    NotStarted,
    Running,
    Ended,
}

#[derive(Clone, Debug, Serialize)]
enum GameEvent {
    Joined(Uuid),
    Started,
    Submitted { player_id: Uuid, answer_id: Uuid },
    NextSlide,
    EndGame,
}

pub struct GameState {
    quiz_id: Uuid,
    status: GameStatus,
    current_slide: i32,
    slide_start_time: SystemTime,
    players: HashMap<Uuid, PlayerState>,
    event_receiver: Receiver<GameEvent>,
    event_sender: Sender<GameEvent>,
}

#[launch]
fn rocket() -> _ {
    use ui::*;

    match fs::create_dir(api::MEDIA_LOCATION) {
        Ok(_) => {}
        Err(e) if e.kind() == std::io::ErrorKind::AlreadyExists => {}
        Err(e) => panic!("{e}"),
    }

    rocket::build()
        .attach(Template::fairing())
        .attach(Database::fairing())
        .manage(Arc::new(RwLock::new(HashMap::<u32, GameState>::new())))
        .mount("/css/", FileServer::from("css"))
        .mount("/img/", FileServer::from("img"))
        .mount("/js/", FileServer::from("js"))
        .mount("/media/", FileServer::from(api::MEDIA_LOCATION))
        .mount(
            "/",
            routes![
                index,
                create_quiz,
                edit_quiz,
                create_public_link,
                present,
                intro,
                participate,
                join_game,
                join_game_form,
                login,
                register,
                favicon,
                game_events,
            ],
        )
        .mount("/api/", {
            use api::*;
            routes![
                set_title,
                add_question,
                next_slide,
                delete_question,
                add_answer,
                can_join,
                set_question_title,
                remove_question_image,
                set_question_slide,
                set_question_image,
                start_game,
                answer_question,
                get_current_slide,
                set_question_true_or_false,
                set_question_type,
                set_correct_answers,
                set_question_duration,
            ]
        })
}
