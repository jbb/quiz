// SPDX-FileCopyrightText: 2022 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use rocket::{get, response::Redirect, uri};

use diesel::prelude::*;

use rocket_dyn_templates::Template;

use uuid::Uuid;

use std::{
    collections::HashMap,
    time::{Duration, SystemTime},
};

use rand::Rng;

use qrcode::{render::svg, QrCode};

use serde::Serialize;

use rocket::tokio::sync::broadcast::channel;

use rocket_ws::WebSocket;

use crate::api::AuthenticatedUser;
use crate::database::{Database, Question};
use crate::*;

#[derive(Serialize)]
struct Context {}

#[get("/create-public-link?<quiz_id>")]
pub async fn create_public_link(db: Database, quiz_id: String) -> Redirect {
    db.create_public_link(Uuid::parse_str(&quiz_id).unwrap())
        .await
        .unwrap();
    Redirect::to("public-links")
}

#[get("/participate?<game_id>&<player_id>&<answer_id>")]
pub async fn participate(
    db: Database,
    game_id: u32,
    player_id: String,
    answer_id: Option<String>,
    state: &GlobalState,
) -> Template {
    let (start_time, quiz_id, current_slide) = {
        let read_state = state.read().unwrap();
        let game = read_state.get(&game_id).unwrap();
        let quiz_id = game.quiz_id;
        let current_slide = game.current_slide;
        let start_time = game.slide_start_time;

        match game.status {
            GameStatus::NotStarted => return Template::render("waiting", Context {}),
            GameStatus::Ended => return Template::render("end", Context {}),
            _ => {}
        }

        (start_time, quiz_id, current_slide)
    };

    let question = db.get_slide(quiz_id, current_slide).await.unwrap();
    let question_id = Uuid::from_slice(&question.id).unwrap();
    let question_anwers = db.get_answers(question_id).await.unwrap();

    let player_id = &Uuid::parse_str(&player_id).unwrap();

    if let Some(answer_id) = answer_id {
        let answer_id = Uuid::parse_str(&answer_id).unwrap();
        let correct = db.get_answer(answer_id).await.unwrap().correct;

        if correct {
            let answer_time = SystemTime::now()
                .duration_since(start_time)
                .unwrap()
                .as_millis();
            let max_answer_time = Duration::from_secs(question.duration as u64).as_millis();
            let max_points = 100;

            let points =
                ((1.0 - (answer_time as f32 / max_answer_time as f32)) * max_points as f32) as u32;
            println!("{points} Points");

            let mut state = state.write().unwrap();
            let game = state.get_mut(&game_id).unwrap();
            let player = game.players.get_mut(player_id).unwrap();

            if !player.answered_questions.contains(&question_id) {
                player.points += points;
                player.answered_questions.push(question_id);
            }
        } else {
            let mut state = state.write().unwrap();
            let game = state.get_mut(&game_id).unwrap();
            let player = game.players.get_mut(player_id).unwrap();

            if !player.answered_questions.contains(&question_id) {
                player.answered_questions.push(question_id);
            }
        }

        let mut state = state.write().unwrap();
        let game = state.get_mut(&game_id).unwrap();

        game.event_sender
            .send(GameEvent::Submitted {
                player_id: *player_id,
                answer_id,
            })
            .expect("Failed to send game event");
    }

    #[derive(Serialize)]
    struct ParticipantContext {
        question: String,
        question_id: String,
        answers: Vec<Answer>,
        game_id: u32,
        duration: i32,
        player: String,
        player_id: String,
        points: u32,
        answered_questions: Vec<String>,
    }

    let read_state = state.read().unwrap();
    let player = read_state
        .get(&game_id)
        .unwrap()
        .players
        .get(player_id)
        .unwrap();
    eprintln!("Answered questions: {:?}", player.answered_questions);

    let context = ParticipantContext {
        question: question.question,
        question_id: question_id.to_string(),
        answers: question_anwers
            .into_iter()
            .map(|a| Answer {
                text: a.text,
                id: Uuid::from_slice(&a.id).unwrap().to_string(),
            })
            .collect(),
        game_id,
        duration: question.duration,
        player: player.name.clone(),
        player_id: player_id.to_string(),
        points: player.points,
        answered_questions: player
            .answered_questions
            .iter()
            .map(|uuid| uuid.to_string())
            .collect::<Vec<String>>(),
    };

    Template::render("participate", context)
}

const PROPERTIES: [&str; 9] = [
    "fast", "slow", "random", "best", "small", "funny", "flying", "winning", "happy",
];
const ANIMALS: [&str; 15] = [
    "Turtle", "Monkey", "Cat", "Dog", "Horse", "Elephant", "Donkey", "Bug", "Duck", "Pig", "Sheep",
    "Human", "Chicken", "Fish", "Bee",
];

fn generate_user_name() -> String {
    let mut rng = ::rand::thread_rng();
    let property = PROPERTIES[rng.gen_range(0..PROPERTIES.len())];
    let animal = ANIMALS[rng.gen_range(0..ANIMALS.len())];
    let number = rng.gen_range(10..99);

    format!("{property}{animal}{number}")
}

#[get("/join")]
pub async fn join_game_form() -> Template {
    Template::render("join", Context {})
}

#[get("/join?<game_id>")]
pub async fn join_game(game_id: u32, state: &GlobalState) -> Redirect {
    let mut writable_state = state.write().unwrap();
    let state = &mut writable_state.get_mut(&game_id).unwrap();

    let player_id = Uuid::new_v4();
    state.players.insert(
        player_id,
        PlayerState {
            name: generate_user_name(),
            points: 0,
            answered_questions: Vec::new(),
        },
    );

    state
        .event_sender
        .send(GameEvent::Joined(player_id))
        .expect("Failed to send join event");

    Redirect::to(uri!(participate(
        game_id,
        player_id.to_string(),
        Option::<String>::None
    )))
}

#[derive(Serialize)]
struct GameContext {
    question: String,
    answers: Vec<Answer>,
    game_id: u32,
    duration: i32,
}

#[get("/present?<game_id>")]
pub async fn present(db: Database, game_id: u32, state: &GlobalState) -> Template {
    let (slide_start_time, quiz_id, current_slide) = {
        let games = state.read().unwrap();
        let state = games.get(&game_id).unwrap();
        let quiz_id = state.quiz_id;
        let current_slide = state.current_slide;
        let start_time = state.slide_start_time;
        (start_time, quiz_id, current_slide)
    };

    let mut question = db.get_slide(quiz_id, current_slide).await.unwrap();

    if SystemTime::now().duration_since(slide_start_time).unwrap()
        > Duration::from_secs(question.duration as u64)
    {
        {
            let mut write_state = state.write().unwrap();
            let game_state = write_state.get_mut(&game_id).unwrap();
            game_state.current_slide += 1;
            game_state.slide_start_time = SystemTime::now();
        }
        let result = db.get_slide(quiz_id, current_slide + 1).await;
        match result {
            Ok(q) => question = q,
            Err(diesel::result::Error::NotFound) => {
                {
                    let mut write_state = state.write().unwrap();
                    let game_state = write_state.get_mut(&game_id).unwrap();
                    game_state.status = GameStatus::Ended;
                }
                return Template::render("end", Context {});
            }
            Err(e) => Err(e).unwrap(),
        }
    }

    let question_anwers = db
        .run(|c| {
            answers::table
                .filter(answers::question_id.eq(question.id))
                .load::<AnswerEntity>(c)
        })
        .await
        .unwrap();

    let context = GameContext {
        question: question.question,
        answers: question_anwers
            .into_iter()
            .map(|a| Answer {
                text: a.text,
                id: Uuid::from_slice(&a.id).unwrap().to_string(),
            })
            .collect(),
        game_id,
        duration: question.duration,
    };

    Template::render("game", context)
}

#[get("/create_game?<quiz_id>")]
pub async fn intro(quiz_id: String, state: &GlobalState) -> Template {
    #[derive(Serialize)]
    struct Context {
        game_id: String,
        qr_code: String,
    }

    let (sender, receiver) = channel(16);

    let game_id = rand::thread_rng().gen_range(0..999999);
    state.write().unwrap().insert(
        game_id,
        GameState {
            quiz_id: Uuid::parse_str(&quiz_id).unwrap(),
            current_slide: 0,
            status: GameStatus::NotStarted,
            slide_start_time: SystemTime::now(),
            players: HashMap::new(),
            event_sender: sender,
            event_receiver: receiver,
        },
    );

    let code = QrCode::new(format!("http://localhost:8000/join?game_id={game_id}")).unwrap();

    let qr_code = code
        .render()
        .min_dimensions(200, 200)
        .dark_color(svg::Color("#00000"))
        .light_color(svg::Color("#ffffff"))
        .build();

    Template::render(
        "intro",
        Context {
            game_id: game_id.to_string(),
            qr_code,
        },
    )
}

#[get("/create")]
pub async fn create_quiz(db: Database) -> Redirect {
    let quiz_id = db.add_quiz("".into()).await.unwrap();

    Redirect::to(uri!(edit_quiz(quiz_id.to_string())))
}

#[get("/edit/<quiz_id>")]
pub async fn edit_quiz(user: AuthenticatedUser, db: Database, quiz_id: &str) -> Template {
    let quiz_uuid = Uuid::parse_str(quiz_id).unwrap();
    let quiz = match db.get_quiz(quiz_uuid).await {
        Ok(quiz) => quiz,
        Err(diesel::NotFound) => return Template::render("error", &ui::Context {}),
        e => {
            e.expect("access database");
            todo!()
        }
    };

    #[derive(Serialize)]
    struct Quiz {
        title: String,
        id: String,
        questions: Vec<Question>,
    }

    #[derive(Serialize)]
    struct Context {
        quiz: Quiz,
        first_question: Question,
    }

    let questions = db.get_questions(quiz_uuid).await;

    Template::render(
        "create-quiz",
        Context {
            first_question: (*questions.first().unwrap()).clone(),
            quiz: Quiz {
                title: quiz.title,
                id: quiz_id.to_string(),
                questions,
            },
        },
    )
}

#[get("/login")]
pub fn login() -> Template {
    Template::render("login", Context {})
}

#[get("/register")]
pub fn register() -> Template {
    Template::render("login", Context {})
}

#[get("/favicon.ico")]
pub fn favicon() -> Redirect {
    Redirect::to(
        "https://www.fu-berlin.de/assets/default2/favicon-12a6f1b0e53f527326498a6bfd4c3abd.ico",
    )
}

#[get("/game/<game_id>/events")]
pub async fn game_events(
    game_id: u32,
    ws: WebSocket,
    state: &GlobalState,
) -> rocket_ws::Channel<'static> {
    use rocket::futures::SinkExt;

    let state = state.inner().clone();

    ws.channel(move |mut stream| {
        Box::pin(async move {
            let mut rx = {
                let mut writable_state = state.write().unwrap();
                let game = writable_state.get_mut(&game_id).unwrap();
                game.event_receiver.resubscribe()
            };

            while let Ok(message) = rx.recv().await {
                let _ = stream
                    .send(rocket_ws::Message::Text(
                        serde_json::to_string_pretty(&message).unwrap(),
                    ))
                    .await;
            }

            Ok(())
        })
    })
}

#[get("/")]
pub fn index() -> Template {
    Template::render("index", Context {})
}
